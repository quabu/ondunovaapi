FROM node:14.16-slim
WORKDIR /ondunova_api
COPY ./package.json /ondunova_api
RUN npm install
COPY . /ondunova_api
EXPOSE 5001
CMD ["node", "index.js"]