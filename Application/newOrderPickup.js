var statusCode = require("../data/Enums/ErrorCodes")

exports.execute = async function(res, req, sqlrep) {
  try {
    let body = req.body;
    let orderPickup = body.orderPickup;

    console.log("*** new Order Pickup ***")
    orderExist = await sqlrep.checkIfOrderExist(orderPickup).catch(function (error){
      throw error
    })

    if (orderExist) {
      console.log("error ya existe")
      res.status(statusCode.statusCode.BAD_REQUEST).send({ message: "La ordre amb nº de palet " + orderPickup.numPalet + " es existent", data: null, success: false})
    } else {
      order = await sqlrep.newOrderPickup(orderPickup).catch(function (error){
        throw error
      })
  
      if (order){
        orders = await sqlrep.getOrders().catch(function (error){
          throw error
        })
        console.log("success")
        res.status(statusCode.statusCode.EXITO).send({ message: "S'ha recollit la ordre correctament", data: orders, success: true})
      } else {
        console.log("error no hay Orders")
        res.status(statusCode.statusCode.BAD_REQUEST).send({ message: "Error al recollir la ordre con nº " + orderPickup.numOf, data: null, success: false})
      }
    }
    
  } catch (e) {
    console.log(e)
    res.status(statusCode.statusCode.SERVER_ERROR).send({ message: "Error de servidor", data: null, success: false})
  }
};