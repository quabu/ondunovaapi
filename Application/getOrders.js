var statusCode = require("../data/Enums/ErrorCodes")

exports.execute = async function(res, req, sqlrep) {
  try {
    console.log("*** Get Orders ***")
    orders = await sqlrep.getOrders().catch(function (error){
      throw error
    })
    console.log("success")
    if (orders.length >0) {
      res.status(statusCode.statusCode.EXITO).send({ message: "Llista de ordre recuperada correctament", data: orders, success: true})
    } else {
      res.status(statusCode.statusCode.EXITO).send({ message: "Llista de ordres buida", data: orders, success: true})
    }
    
  } catch (e) {
    console.log(e)
    res.status(statusCode.statusCode.SERVER_ERROR).send({ message: "Error de servidor", data: null, success: false})
  }
};