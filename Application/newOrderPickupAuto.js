var statusCode = require("../data/Enums/ErrorCodes")

exports.execute = async function(res, req, sqlrep) {
  try {
    let body = req.body;
    let orderPickup = body.orderPickup;

    console.log("*** New Order Pickup Auto ***")
    if (!orderPickup.numOf.includes("ERROR")) {
      console.log("**** NO ERROR ***")
      let orderValidated = await sqlrep.validateOrder(orderPickup.numOf, orderPickup.jobNumber).catch(function (error){
        throw error
      })
      if (orderValidated.recordset.length > 0) {
        orderExist = await sqlrep.checkIfOrderExist(orderPickup).catch(function (error){
          throw error
        })
        if (orderExist) {
          console.log("error  entrada ya existe")
          res.status(statusCode.statusCode.BAD_REQUEST).send({ message: "La ordre amb nº de palet " + orderPickup.numPalet + " es existent", data: null, success: false})
        } else {
          order = await sqlrep.newOrderPickupAuto(orderPickup, orderValidated.recordset[0]).catch(function (error){
            throw error
          })
      
          if (order){
            orders = await sqlrep.getOrders().catch(function (error){
              throw error
            })
            console.log("success")
            res.status(statusCode.statusCode.EXITO).send({ message: "Se ha recollit la ordre correctament", data: orders, success: true})
          } else {
            console.log("error no hay Orders")
            res.status(statusCode.statusCode.BAD_REQUEST).send({ message: "Error al recollir la ordre amb nº " + orderPickup.numOf, data: null, success: false})
          }
        }
      } else {
        console.log("error order not validated")
        res.status(statusCode.statusCode.BAD_REQUEST).send({ message: "Ordre no valida", data: null, success: false})
      }
    } else {
      console.log("**** IS ERROR ***")
      orderExist = await sqlrep.checkIfOrderExist(orderPickup).catch(function (error){
        throw error
      })
      if (orderExist) {
        console.log("error entrada ya existe")
        res.status(statusCode.statusCode.BAD_REQUEST).send({ message: "La ordre amb nº de palet " + orderPickup.numPalet + " es existent", data: null, success: false})
      } else {
        order = await sqlrep.newOrderPickupAuto(orderPickup, null).catch(function (error){
          throw error
        })
    
        if (order){
          orders = await sqlrep.getOrders().catch(function (error){
            throw error
          })
          console.log("success")
          res.status(statusCode.statusCode.EXITO).send({ message: "Se ha recollit la ordre correctament", data: orders, success: true})
        } else {
          console.log("error no hay Orders")
          res.status(statusCode.statusCode.BAD_REQUEST).send({ message: "Error al recollir la ordre amb nº " + orderPickup.numOf, data: null, success: false})
        }
      }
    } 
  } catch (e) {
    console.log(e)
    res.status(statusCode.statusCode.SERVER_ERROR).send({ message: "Error de servidor", data: null, success: false})
  }
};