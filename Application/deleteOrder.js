var statusCode = require("../data/Enums/ErrorCodes")

exports.execute = async function(res, req, sqlrep) {
  try {
    let body = req.body;
    let orderId = body.orderId;
    console.log("*** Delete Order ***")
    order = await sqlrep.deleteOrder(orderId).catch(function (error){
      throw error
    })
    if(order){
      console.log("success")
      orders = await sqlrep.getOrders().catch(function (error){
        throw error
      })
      res.status(statusCode.statusCode.EXITO).send({ message: "Ordre eliminada correctament", data: orders, success: true})
    } else {
      console.log("error")
      res.status(statusCode.statusCode.BAD_REQUEST).send({ message: "Error al eliminar l'ordre", data: null, success: false})
    }
  } catch (e) {
    console.log(e)
    res.status(statusCode.statusCode.SERVER_ERROR).send({ message: "Error de servidor", data: null, success: false});
  }
};