var statusCode = require("../data/Enums/ErrorCodes")

exports.execute = async function(res, req, sqlrep) {
    let init = new Date();
  try {
    let body = req.body;
    let numOf = body.numOf;
    console.log("*** Get Position Suggestion ***")
    positionSuggestion = await sqlrep.getPositionSuggestion(numOf).catch(function (error){
      throw error
    })
    if (positionSuggestion[0]){
      console.log("success")
      res.status(statusCode.statusCode.EXITO).send({ message: "Posició recuperada correctament", data: positionSuggestion, success: true})
    } else {
      console.log("error")
      res.status(statusCode.statusCode.BAD_REQUEST).send({ message: "No hay sugerencia de posició", data: null, success: false});
    }
  } catch (e) {
    console.log(e)
    res.status(statusCode.statusCode.SERVER_ERROR).send({ message: "Error de servidor", data: null, success: false})
  }
};