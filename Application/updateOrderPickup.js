var statusCode = require("../data/Enums/ErrorCodes")

exports.execute = async function(res, req, sqlrep) {
  try {
    let body = req.body;
    let orderPickup = body.orderPickup;
    console.log("*** Update Order Pickup ***")
    orderCanUpdate = await sqlrep.checkIfOrderCanUpdate(orderPickup).catch(function (error){
      throw error
    })
    
    if (orderCanUpdate){
      order = await sqlrep.updateOrderPickup(orderPickup).catch(function (error){
        throw error
      })  
      if (order){
        orders = await sqlrep.getOrders().catch(function (error){
          throw error
        })
        console.log("success")
        res.status(statusCode.statusCode.EXITO).send({ message: "Ordre actualitzada correctament", data: orders, success: true})
      } else {
        console.log("error")
        res.status(statusCode.statusCode.BAD_REQUEST).send({ message: "Error al actualizar Ordre", data: null, success: false})
      }      
    } else {
      orderExist = await sqlrep.checkIfOrderExist(orderPickup).catch(function (error){
        throw error
      })
      if (orderExist) {
        console.log("error")
        res.status(statusCode.statusCode.BAD_REQUEST).send({ message: "La ordre amb nº de palet " + orderPickup.numPalet + " es existent", data: null, success: false})
      } else {
        order = await sqlrep.updateOrderPickup(orderPickup).catch(function (error){
          throw error
        })  
        if (order){
          orders = await sqlrep.getOrders().catch(function (error){
            throw error
          })
          console.log("success")
          res.status(statusCode.statusCode.EXITO).send({ message: "Ordre actualitzada correctament", data: orders, success: true})
        } else {
          console.log("error")
          res.status(statusCode.statusCode.BAD_REQUEST).send({ message: "Error al actualizar Ordre", data: null, success: false})
        }      
      }
    }
  } catch (e) {
    console.log(e)
    res.status(statusCode.statusCode.SERVER_ERROR).send({ message: "Error de servidor", data: null, success: false});
  }
};