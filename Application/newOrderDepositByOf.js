var statusCode = require("../data/Enums/ErrorCodes")

exports.execute = async function(res, req, sqlrep) {
  try {
    let body = req.body;
    let numOf = body.numOf;
    console.log("*** New Order Deposit by OF***")
    order = await sqlrep.newOrderDepositByOf(numOf).catch(function (error){
      throw error
    })

    if(order){
      /* orders = await sqlrep.getOrders().catch(function (error){
        throw error
      }) */
      console.log("success")
      res.status(statusCode.statusCode.EXITO).send({ message: "Ordre depositada correctament", data: [], success: true})
    } else {
      console.log("error")
      res.status(statusCode.statusCode.BAD_REQUEST).send({ message: "No s'ha realizat el deposit correctament", data: null, success: false})
    }
  } catch (e) {
    console.log(e)
    res.status(statusCode.statusCode.SERVER_ERROR).send({ message: "Error de servidor", data: null, success: false})
  }
};