var statusCode = require("../data/Enums/ErrorCodes")

exports.execute = async function(res, req, sqlrep) {
  try {
    let body = req.body;
    let orderNumber = body.order;
    let jobNumber = body.jobNumber;
    
    console.log("*** Validate Order ***")
    order = await sqlrep.validateOrder(orderNumber, jobNumber).catch(function (error){
      throw error
    })

    if (order.recordset.length > 0){
      console.log("success")
      res.status(statusCode.statusCode.EXITO).send({ message: "Orden validada", data: order.recordset, success: true})
    } else {
      console.log("error")
      res.status(statusCode.statusCode.EXITO).send({ message: "La orden con nº de of " + orderNumber + " no existe", data: null, success: false})
    }
  } catch (e) {
    console.log(e)
    res.status(statusCode.statusCode.SERVER_ERROR).send({ message: "Error de servidor", data: null, success: false});
  }
};