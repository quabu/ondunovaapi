var statusCode = require("../data/Enums/ErrorCodes")

exports.execute = async function(res, req, sqlrep) {
  try {
    let body = req.body;
    let numOf = body.numOf;
    console.log("*** Get Order By Of ***")
    orders = await sqlrep.getOrdersByOf(numOf).catch(function (error){
      throw error
    })
    console.log("success")
    if (orders.length > 0){
      res.status(statusCode.statusCode.EXITO).send({ message: "Ordres recuperadas correctament", data: orders, success: true})
    } else {
      res.status(statusCode.statusCode.BAD_REQUEST).send({ message: "No s'han trobat ordres amb num OF" + numOf, data: null, success: false})
    }
    
  } catch (e) {
    console.log(e)
    res.status(statusCode.statusCode.SERVER_ERROR).send({ message: "Error de servidor", data: null, success: false})
  }
};