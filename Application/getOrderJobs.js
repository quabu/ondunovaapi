var statusCode = require("../data/Enums/ErrorCodes")

exports.execute = async function(res, req, sqlrep) {
  try {
    console.log("*** Get Order Jobs ***")
    order = await sqlrep.getOrderJobs().catch(function (error){
      throw error
    })
    console.log("success")
    if (order.recordset.length > 0){
        res.status(statusCode.statusCode.EXITO).send({ message: "Llista de ordres recuperada correctament", data: order.recordset, success: true})
    } else {
        res.status(statusCode.statusCode.EXITO).send({ message: "La llista de ordres esta buidaa", data: [], success: true})
    }
  } catch (e) {
    console.log(e)
    res.status(statusCode.statusCode.SERVER_ERROR).send({ message: "Error de servidor", data: null, success: true})
  }
};