var statusCode = require("../data/Enums/ErrorCodes")

exports.execute = async function(res, req, sqlrep) {
  try {
    let body = req.body;
    let orderStatus = body.orderStatus;
    let orderId = body.orderId;

    console.log("*** Update Order Status ***")
    order = await sqlrep.updateOrderSatusById(orderStatus, orderId).catch(function (error){
      throw error
    })  
      if (order){
        orders = await sqlrep.getOrders().catch(function (error){
          throw error
        })
        console.log("success")
        res.status(statusCode.statusCode.EXITO).send({ message: "Estat actualitzat correctament", data: orders, success: true})
      } else {
        console.log("error")
        res.status(statusCode.statusCode.BAD_REQUEST).send({ message: "Error al actualitzar estat", data: null, success: false})
      }
    
  } catch (e) {
    console.log(e)
    res.status(statusCode.statusCode.SERVER_ERROR).send({ message: "Error de servidor", data: null, success: false});
  }
};