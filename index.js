var express = require('express')
var bodyParser = require('body-parser')

var fs = require('fs');
var http = require('http');
var https = require('https');

var env = require("./env/ondunova_env")

var cors = require('cors')

var MySQLrepository = require("./Infrastructure/MySQLrepository");
//PCTOP
var validateOrder = require("./Application/validateOrder")
var getOrderJobs = require("./Application/getOrderJobs")

//Ondunova_DB
var getOrders = require("./Application/getOrders")
var getOrdersByOf = require("./Application/getOrdersByOf")
var deleteOrder = require("./Application/deleteOrder")

//OrderPickup
var newOrderPickupAuto = require("./Application/newOrderPickupAuto")
var newOrderPickup = require("./Application/newOrderPickup")
var updateOrderPickup = require("./Application/updateOrderPickup")
var updateOrderSatusById = require("./Application/updateOrderSatusById")
var getPositionSuggestion = require("./Application/getPositionSuggestion")

//OrderDeposit
var newOrderDeposit = require("./Application/newOrderDeposit")
var newOrderDepositByOf = require("./Application/newOrderDepositByOf")

//Ondunova
var ondunova = require('./Infrastructure/ondunovaController')
var app = express()

app.use(bodyParser.urlencoded({extended:false}))
app.use(bodyParser.json())
app.use(cors())

//PCT
app.post("/validateOrder", function (req, res) {
    let sqlrep = new MySQLrepository()
    validateOrder.execute(res, req, sqlrep);
});

app.get("/getOrderJobs", function (req, res) {
    let sqlrep = new MySQLrepository()
    getOrderJobs.execute(res, req, sqlrep);
});

//OndunovaDB
app.get("/getOrders", function (req, res)  {
    let sqlrep = new MySQLrepository()
    getOrders.execute(res, req, sqlrep);
});

app.post("/getOrdersByOf", function (req, res)  {
    let sqlrep = new MySQLrepository()
    getOrdersByOf.execute(res, req, sqlrep);
});

app.post("/deleteOrder", function (req, res)  {
    let sqlrep = new MySQLrepository()
    deleteOrder.execute(res, req, sqlrep);
});

//OrderPickup
app.post("/newOrderPickupAuto", function (req, res)  {
    let sqlrep = new MySQLrepository()
    newOrderPickupAuto.execute(res, req, sqlrep);
});

app.post("/newOrderPickup", function (req, res)  {
    let sqlrep = new MySQLrepository()
    newOrderPickup.execute(res, req, sqlrep);
});

app.post("/updateOrderPickup", function (req, res)  {
    let sqlrep = new MySQLrepository()
    updateOrderPickup.execute(res, req, sqlrep);
});

app.post("/updateOrderStatusById", function (req, res)  {
    let sqlrep = new MySQLrepository()
    updateOrderSatusById.execute(res, req, sqlrep);
});

app.post("/getPositionSuggestion", function (req, res)  {
    let sqlrep = new MySQLrepository()
    getPositionSuggestion.execute(res, req, sqlrep);
});

//OrderDeposit
app.post("/newOrderDeposit", function (req, res){
    let sqlrep = new MySQLrepository()
    newOrderDeposit.execute(res, req, sqlrep);
});

app.post("/newOrderDepositByOf", function (req, res){
    let sqlrep = new MySQLrepository()
    newOrderDepositByOf.execute(res, req, sqlrep);
});



app.listen(5001, () => {
    console.log("Server running on port 5001");
});

/* app.use(cors())
app.use("/ondunova", ondunova) */

/* if (env.environment == "development"){
    var httpsServer = https.createServer(credentials, app);
    console.log("HTTPS")
    httpsServer.listen(3000), () => {console.log('Server is running on port 3000 by https')};
} else {
    var httpServer = http.createServer(app);
    console.log("HTTP")
    httpServer.listen(3000), () => {console.log('Server is running on port 3000 by http')};
} */