CREATE USER 'ondunova'@'localhost' IDENTIFIED BY 'ondunova2021';
GRANT ALL PRIVILEGES ON *.* TO 'ondunova'@'localhost';
FLUSH PRIVILEGES;

CREATE TABLE `gestionAlmacen` (
	id INT auto_increment NOT NULL,
	numOf varchar(50) NOT NULL,
	numPalet INT NOT NULL,
	totalPalets INT NULL,
    quantity INT NOT NULL,
    storePosition varchar(50) NULL,
	orderStatus varchar(50) DEFAULT 'EnAlmacen' NOT NULL,
    user varchar(50) NULL,
	pickupDate DATETIME NULL,
	depositDate DATETIME NULL,
	machineId varchar(50) NULL,
	machineName varchar(50) NULL,
	clientId varchar(50) NULL,
	clientName varchar(50) NULL,
	origin varchar(50) NULL,
	CONSTRAINT user_PK PRIMARY KEY (id)
);

