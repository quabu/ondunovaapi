const sql = require('mssql')  
var Connection = require('tedious').Connection;
var Request = require('tedious').Request;

const config = {  
    user: 'quabu_sql',
    password: 'quabuSQL21',
    server: '172.16.20.26',
    database: 'pctoppexport_onv_vilafranca',
    options: {  
        encrypt: false,
        instanceName: "PCT",
        enableArithAbort: true,
    }
}   
const poolPromise = new sql.ConnectionPool(config)  
.connect()  
.then(pool => {  
    console.log('Connected to MSSQL')  
    return pool  
})  
.catch(err => console.log('Database Connection Failed! Bad Config: ', err))  

module.exports = { sql, poolPromise }  
