

var mysql = require("mysql");
var sql = require("mssql");
var Connection = require('tedious').Connection;
var Request = require('tedious').Request;

var dateFunctions = require("../Utils/dateFunctions");

module.exports = class MySQLrepository {
    constructor() {}
    //PCTOP
    validateOrder(orderNumber, jobNumber){
      try {        
        return new Promise(function (resolve, reject) {
        const config = {  
          user: 'quabu_sql',
          password: 'quabuSQL21',
          server: '172.16.20.26',
          database: 'pctoppexport_onv_vilafranca',
          options: {  
              encrypt: false,
              instanceName: "PCT",
              enableArithAbort: true,
          }
        }   

        const poolPromise = new sql.ConnectionPool(config).connect().then(pool => { 

          pool.request()
          .input('orderNumber', sql.NVarChar, orderNumber)
          .input('jobNumber', sql.NVarChar , jobNumber)
          .query(
            "SELECT TOP(1) OrderNumber, JobNumber, QuantityOrdered, QuantityToProduce, QuantityPerPallet, NoPallets, "
            + "OrderJobs.StartDate, "
            + "OrderJobs.DisplayStatus, "
            + "MachineDefinition.Name as MachineName, "
            + "MachineDefinition.MachineCode, "
            + "Customers.Number as ClientId, "
            + "Customers.Name as ClientName "
            + "FROM Orders as Orders "
            + "INNER JOIN Customers as Customers ON Orders.CustomerID = Customers.ID "
            + "LEFT JOIN OrderJobs as OrderJobs ON Orders.ID = OrderJobs.OrderID "
            + "LEFT JOIN MachineDefinition as MachineDefinition ON OrderJobs.MachineID = MachineDefinition.ID " 
            + "WHERE MachineDefinition.MachineCode IN ('2U', '2S', '2C', '2B','2R','2J') AND OrderNumber = @orderNumber AND Orders.JobNumber = @jobNumber "
            + "ORDER BY OrderJobs.sequence ASC", 
            function(error, results){
              if (error) {
                console.log(error);
                reject(error);
              }
              resolve(results);
              pool.close();
            })  
        }).catch(err => console.log('Database Connection Failed! Bad Config: ', err))  
        });
      } catch (error) {
        throw error;
      }
    }

    getOrderJobs(){
      try {
        return new Promise(function (resolve, reject) {
          const query = "SELECT Orders.OrderNumber, "
          + "Orders.JobNumber, " 
          + "Orders.QuantityOrdered, "
          + "Orders.QuantityToProduce, "
          + "Orders.QuantityPerPallet, "
          + "Orders.NoPallets, "
          + "OrderJobs.StartDate, "
          + "OrderJobs.DisplayStatus, "
          + "MachineDefinition.Name as MachineName, "
          + "MachineDefinition.MachineCode, "
          + "Customers.Number as ClientId, "
          + "Customers.Name as ClientName "
      + "FROM Orders as Orders "
      + "INNER JOIN OrderJobs as OrderJobs ON Orders.ID = OrderJobs.OrderID "
      + "INNER JOIN MachineDefinition as MachineDefinition ON OrderJobs.MachineID = MachineDefinition.ID "
      + "INNER JOIN Customers as Customers ON Orders.CustomerID = Customers.ID "
      + "WHERE MachineDefinition.MachineCode IN ('2U', '2S', '2C', '2B','2R','2J') AND OrderJobs.DisplayStatus IN('C1', 'C0', 'B2')  ORDER BY OrderJobs.StartDate ASC;"
          const config = {  
            user: 'quabu_sql',
            password: 'quabuSQL21',
            server: '172.16.20.26',
            database: 'pctoppexport_onv_vilafranca',
            options: {  
                encrypt: false,
                instanceName: "PCT",
                enableArithAbort: true,
            }
          }   
          const poolPromise = new sql.ConnectionPool(config).connect().then(pool => { 
  
            pool.query(query, 
              function(error, results){
                if (error) {
                  console.log(error);
                  reject(error);
                }
                resolve(results);
                pool.close();
              })  
          }).catch(err => console.log('Database Connection Failed! Bad Config: ', err))        
        });
      } catch (error) {
        throw error;
      }
    }

    //ondunova_db
    getOrders() {
        try {
    
          return new Promise(function (resolve, reject) {
            //Connect with database MySQL
            var connection = mysql.createConnection({
              host: "ondunova_db",
              user: "quabu",
              password: "ondunova2021",
              database: "ondunova",
            });
            connection.beginTransaction(function (err) {
              if (err) {
                throw err;
              }
              connection.query(
                "SELECT * FROM gestionAlmacen ORDER BY pickupDate DESC",
                function (error, results) {
                  if (error) {
                    console.log(error);
                    reject(error);
                  }
                  resolve(results);
                  connection.end();
                }
              );
            });
          });
        } catch (error) {
          throw error;
        }
    }

    getOrdersByOf(numOf) {
      try {
        let values = [
          numOf
        ]
        return new Promise(function (resolve, reject) {
          //Connect with database MySQL
          var connection = mysql.createConnection({
            host: "ondunova_db",
            user: "quabu",
            password: "ondunova2021",
            database: "ondunova",
          });
          connection.beginTransaction(function (err) {
            if (err) {
              throw err;
            }
            connection.query(
              "SELECT * FROM gestionAlmacen WHERE numOf = ? ORDER BY pickupDate DESC",
              values,
              function (error, results) {
                if (error) {
                  console.log(error);
                  reject(error);
                }
                resolve(results);
                connection.end();
              }
            );
          });
        });
      } catch (error) {
        throw error;
      }
  }

    deleteOrder(orderId){
      try {
        let values = [
          orderId
        ];

        return new Promise(function (resolve, reject) {
          //Connect with database MySQL
          var connection = mysql.createConnection({
            host: "ondunova_db",
            user: "quabu",
            password: "ondunova2021",
            database: "ondunova",
          });
          console.log(values)
          connection.beginTransaction(function (err) {
            if (err) {
              throw err;
            }
            connection.query(
              "DELETE FROM gestionAlmacen WHERE id = ?",
              values,
              function (error, results) {
                if (error) {
                  return connection.rollback(function () {
                    reject(error);
                  });
                }
                connection.commit(function (err) {
                  if (err) {
                    return connection.rollback(function () {
                      reject(err);
                    });
                  }
                  
                });
                console.log(results);
                console.log(results.affectedRows)
                if (results.affectedRows > 0){
                  resolve(true);
                  console.log("success!");
                } else {
                  resolve(false)
                  console.log("Error al eliminar el palet");
                }
                connection.end();
              }
            );
          });
        });
      } catch (error) {
        throw error;
      }
    }

    checkIfOrderExist(orderPickup){
      try {
        let values = [
          orderPickup.numOf,
          orderPickup.numPalet
        ];

        return new Promise(function (resolve, reject) {
          //Connect with database MySQL
          var connection = mysql.createConnection({
            host: "ondunova_db",
            user: "quabu",
            password: "ondunova2021",
            database: "ondunova",
          });
          console.log("********* VALUES *************")
          console.log(values)
          connection.beginTransaction(function (err) {
            if (err) {
              throw err;
            }
              connection.query(
                "SELECT * FROM gestionAlmacen WHERE numOf = ? AND numPalet = ?",
                values,
                function (error, results) {
                  console.log(results)
                  if (error) {
                    return connection.rollback(function () {
                      reject(error);
                    });
                  }
                  if (results.length > 0){
                    resolve(true);
                  } else {
                    resolve(false);
                  }
                  connection.end();
                }
              );
          });
        });
      } catch (error) {
        throw error;
      }
    }

    checkIfOrderCanUpdate(orderPickup){
      try {
        let values = [
          orderPickup.id,
          orderPickup.numOf,
          orderPickup.numPalet
        ];

        return new Promise(function (resolve, reject) {
          //Connect with database MySQL
          var connection = mysql.createConnection({
            host: "ondunova_db",
            user: "quabu",
            password: "ondunova2021",
            database: "ondunova",
          });
          console.log("********* VALUES *************")
          console.log(values)
          connection.beginTransaction(function (err) {
            if (err) {
              throw err;
            }
              connection.query(
                "SELECT * FROM gestionAlmacen WHERE id = ? AND numOf = ? AND numPalet = ? ",
                values,
                function (error, results) {
                  console.log(results)
                  if (error) {
                    return connection.rollback(function () {
                      reject(error);
                    });
                  }
                  if (results.length > 0){
                    resolve(true);
                  } else {
                    resolve(false);
                  }
                  connection.end();
                }
              );
          });
        });
      } catch (error) {
        throw error;
      }
    }

    newOrderPickupAuto(orderPickup, orderValidated){
      try {
        let dateF = new dateFunctions()
        let date = dateF.addHoursToDate(new Date(), 1)
        let fields = "(numOf, numPalet, totalPalets, quantity, orderStatus, pickupDate, machineId, machineName, clientId, clientName, origin) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
        let values = []
        if (!orderPickup.numOf.includes("ERROR")) {
          values = [
            orderPickup.numOf,
            orderPickup.numPalet,
            orderValidated.NoPallets,
            orderPickup.quantity,
            "EnRecogida",
            date,
            orderValidated.MachineCode,
            orderValidated.MachineName,
            orderValidated.ClientId,
            orderValidated.ClientName,
            orderPickup.origin
          ];
        } else {
          values = [
            orderPickup.numOf,
            orderPickup.numPalet,
            null,
            orderPickup.quantity,
            "EnRecogida",
            date,
            null,
            null,
            null,
            null,
            orderPickup.origin
          ];
        }

        return new Promise(function (resolve, reject) {
          //Connect with database MySQL
          var connection = mysql.createConnection({
            host: "ondunova_db",
            user: "quabu",
            password: "ondunova2021",
            database: "ondunova",
          });
          connection.beginTransaction(function (err) {
            if (err) {
              throw err;
            }
            connection.query(
              "INSERT INTO gestionAlmacen" + " " + fields,
              values,
              function (error, results) {
                console.log(results)
                if (error) {
                  return connection.rollback(function () {
                    reject(error);
                  });
                }
                connection.commit(function (err) {
                  if (err) {
                    return connection.rollback(function () {
                      reject(err);
                    });
                  }
                  console.log("success!");
                });
                resolve(true);
                connection.end();
              }
            );
          });
        });
      } catch (error) {
        throw error;
      }
    }

    newOrderPickup(orderPickup){
      try {
        let dateF = new dateFunctions()
        let date = dateF.addHoursToDate(new Date(), 1)
        let fields = "(numOf, numPalet, totalPalets, quantity, storePosition, orderStatus, pickupDate, machineId, machineName, clientId, clientName, origin) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
        let values = [
          orderPickup.numOf,
          orderPickup.numPalet,
          orderPickup.totalPalets,
          orderPickup.quantity,
          orderPickup.storePosition,
          "EnAlmacen",
          date,
          orderPickup.machineId,
          orderPickup.machineName,
          orderPickup.clientId,
          orderPickup.clientName,
          orderPickup.origin
        ];

        return new Promise(function (resolve, reject) {
          //Connect with database MySQL
          var connection = mysql.createConnection({
            host: "ondunova_db",
            user: "quabu",
            password: "ondunova2021",
            database: "ondunova",
          });
          console.log("********* VALUES *************")
          console.log(values)
          connection.beginTransaction(function (err) {
            if (err) {
              throw err;
            }
            connection.query(
              "INSERT INTO gestionAlmacen" + " " + fields,
              values,
              function (error, results) {
                console.log(results)
                if (error) {
                  return connection.rollback(function () {
                    reject(error);
                  });
                }
                connection.commit(function (err) {
                  if (err) {
                    return connection.rollback(function () {
                      reject(err);
                    });
                  }
                  console.log("success!");
                });
                resolve(true);
                connection.end();
              }
            );
          });
        });
      } catch (error) {
        throw error;
      }
    }

    newOrderPickupParameter() {
      
    }

    updateOrderPickup(orderPickup){
      try{
        let dateF = new dateFunctions()
        let date = dateF.addHoursToDate(new Date(), 1)
        let updateValues = [
          orderPickup.numOf,
          orderPickup.numPalet,
          orderPickup.quantity,
          orderPickup.totalPalets,
          orderPickup.storePosition,
          orderPickup.orderStatus,
          date,
          orderPickup.machineId,
          orderPickup.machineName,
          orderPickup.clientId,
          orderPickup.clientName,
          orderPickup.origin,
          orderPickup.id,
        ];

        return new Promise(function (resolve, reject) {
          //Connect with database MySQL
          var connection = mysql.createConnection({
            host: "ondunova_db",
            user: "quabu",
            password: "ondunova2021",
            database: "ondunova",
          });
          console.log("********* VALUES *************")
          console.log(updateValues)
          connection.beginTransaction(function (err) {
            if (err) {
              throw err;
            }

            connection.query(
              "UPDATE gestionAlmacen SET numOf = ?, numPalet = ?,quantity = ?, totalPalets = ?, storePosition = ?, orderStatus = ?, pickupDate = ?, machineId = ?, "
              + "machineName = ?, clientId = ?, clientName = ?, origin = ? WHERE id = ?",
              updateValues,
              function (error, results) {
                console.log(results)
                if (error) {
                  return connection.rollback(function () {
                    reject(error);
                  });
                }
                connection.commit(function (err) {
                  if (err) {
                    return connection.rollback(function () {
                      reject(err);
                    });
                  }
                  console.log("success!");
                });
                resolve(true);
                connection.end();
              });
          });
        });
      } catch (error) {
        throw error;
      }
    }

    updateOrderSatusById(orderStatus, orderId){
      try{

        let values = [
          orderStatus,
          orderId
        ];

        return new Promise(function (resolve, reject) {
          //Connect with database MySQL
          var connection = mysql.createConnection({
            host: "ondunova_db",
            user: "quabu",
            password: "ondunova2021",
            database: "ondunova",
          });
          connection.beginTransaction(function (err) {
            if (err) {
              throw err;
            }

            connection.query(
              "UPDATE gestionAlmacen SET orderStatus = ? WHERE id = ?",
              values,
              function (error, results) {
                console.log(results)
                if (error) {
                  return connection.rollback(function () {
                    reject(error);
                  });
                }
                connection.commit(function (err) {
                  if (err) {
                    return connection.rollback(function () {
                      reject(err);
                    });
                  }
                  console.log("success!");
                });
                resolve(true);
                connection.end();
              });
          });
        });
      } catch (error) {
        throw error;
      }
    }

    getPositionSuggestion(numOf){
      try {
        let values = [
          numOf,
          "EnRecogida",
          "EnTransito",
        ]
        return new Promise(function (resolve, reject) {
          //Connect with database MySQL
          var connection = mysql.createConnection({
            host: "ondunova_db",
            user: "quabu",
            password: "ondunova2021",
            database: "ondunova",
          });
          connection.beginTransaction(function (err) {
            if (err) {
              throw err;
            }
            connection.query(
              "SELECT storePosition FROM gestionAlmacen WHERE numOf = ? AND orderStatus NOT IN ( ?, ? ) ORDER BY pickupDate DESC LIMIT 1",
              values,
              function (error, results) {
                if (error) {
                  console.log(error);
                  reject(error);
                }
                resolve(results);
                connection.end();
              }
            );
          });
        });
      } catch (error) {
        throw error;
      }
    }

    newOrderDeposit(orderDeposit){
      try {
        let dateF = new dateFunctions()
        let date = dateF.addHoursToDate(new Date(), 1)
        let values = [
          null,
          "EnProduccion",
          date,
          orderDeposit.id,
        ];

        return new Promise(function (resolve, reject) {
          //Connect with database MySQL
          var connection = mysql.createConnection({
            host: "ondunova_db",
            user: "quabu",
            password: "ondunova2021",
            database: "ondunova",
          });
          console.log(values)
          connection.beginTransaction(function (err) {
            if (err) {
              throw err;
            }
            connection.query(
              "UPDATE gestionAlmacen SET storePosition = ?,orderStatus = ?, depositDate = ? WHERE id = ?",
              values,
              function (error, results) {
                if (error) {
                  return connection.rollback(function () {
                    reject(error);
                  });
                }
                connection.commit(function (err) {
                  if (err) {
                    return connection.rollback(function () {
                      reject(err);
                    });
                  }
                  
                });
                console.log(results.changedRows)
                if (results.changedRows > 0){
                  resolve(true);
                  console.log("success!");
                } else {
                  resolve(false)
                  console.log("update not correct");
                }
                connection.end();
              }
            );
          });
        });
      } catch (error) {
        throw error;
      }
    }

    newOrderDepositByOf(numOf){
      try{
        let dateF = new dateFunctions()
        let date = dateF.addHoursToDate(new Date(), 1)
        let values = [
          null,
          "EnProduccion",
          date,
          numOf
        ];

        return new Promise(function (resolve, reject) {
          //Connect with database MySQL
          var connection = mysql.createConnection({
            host: "ondunova_db",
            user: "quabu",
            password: "ondunova2021",
            database: "ondunova",
          });
          console.log("********* VALUES *************")
          console.log(values)
          connection.beginTransaction(function (err) {
            if (err) {
              throw err;
            }

            connection.query(
              "UPDATE gestionAlmacen SET storePosition = ?, orderStatus = ?, depositDate = ? WHERE numOf = ?",
              values,
              function (error, results) {
                console.log(results)
                if (error) {
                  return connection.rollback(function () {
                    reject(error);
                  });
                }
                connection.commit(function (err) {
                  if (err) {
                    return connection.rollback(function () {
                      reject(err);
                    });
                  }
                  console.log("success!");
                });
                console.log(results.changedRows)
                if (results.changedRows > 0){
                  resolve(true);
                  console.log("success!");
                } else {
                  resolve(false)
                  console.log("update not correct");
                }
                connection.end();
              });
          });
        });
      } catch (error) {
        throw error;
      }
    }
};
