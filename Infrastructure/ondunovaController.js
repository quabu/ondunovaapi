
var express = require("express");
var router = express.Router();
var MySQLrepository = require("./MySQLrepository");
//PCTOP
var validateOrder = require("../Application/validateOrder")
//Ondunova_DB
var getOrders = require("../Application/getOrders")
var newOrderPickup = require("../Application/newOrderPickup")
var newOrderDeposit = require("../Application/newOrderDeposit")



router.get("/validateOrder", function (req, res) {
    let sqlrep = new MySQLrepository()
    validateOrder.execute(res, req, sqlrep);
});

router.get("/getOrderJobs", function (req, res) {
    let sqlrep = new MySQLrepository()
    validateOrder.execute(res, req, sqlrep);
});

router.get("/getOrders", function (req, res)  {
    let sqlrep = new MySQLrepository()
    getOrders.execute(res, req, sqlrep);
});

router.post("/deleteOrder", function (req, res)  {
    let sqlrep = new MySQLrepository()
    deleteOrder.execute(res, req, sqlrep);
});

router.post("/newOrderPickup", function (req, res)  {
    let sqlrep = new MySQLrepository()
    newOrderPickup.execute(res, req, sqlrep);
});

router.post("/newOrderDeposit", function (req, res){
    let sqlrep = new MySQLrepository()
    newOrderDeposit.execute(res, req, sqlrep);
});

module.exports = router;